import json
import logging

logger = logging.getLogger(__name__)

config = None

with open('config/config.json', 'r') as f:
    config = json.load(f)


def get(*keys):
    if config is None:
        logger.warning('No configuration loaded')
        return None

    obj = None
    for key in keys:
        logger.debug(f'Fetching {key}')
        try:
            if obj is None:
                obj = config[key]
            else:
                obj = obj[key]
        except KeyError:
            logger.debug(f'No key: {key} in {obj}')
            return None
    return obj


def get_int(*keys):
    s = get(keys)
    if s is None:
        return None
    else:
        return int(s)

