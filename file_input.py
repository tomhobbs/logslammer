import config
import logging
import logging.config
import signal
import sys

import service.file_watcher as fw

logging.config.fileConfig('config/logging.conf')
logger = logging.getLogger(__name__)

threads = []

def signal_handler(signal, frame):
    logger.info('Shutting down');
    global threads
    for thread in threads:
        thread.stop()
    sys.exit(0)


if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)

    to_watch = config.get('watch')
    for watch in to_watch:
        path = watch['path'] + '/' + watch['file']
        t = fw.Tail(watch['name'], path)
        t.start()
        threads.append(t)

    while True:
        signal.pause()
