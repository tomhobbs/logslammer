import logging
import pymongo

import config
from utils import current_time_millis


logger = logging.getLogger(__name__)

db = None

if db is None:
    connection = pymongo.MongoClient(host=config.get('mongo', 'host'),
                                     port=config.get_int('mongo', 'port'))
    db = connection[config.get('mongo', 'db')]


def save(log):
    logger.info(f'Saving {log}')
    log['createdAt'] = current_time_millis()
    db['log'].insert_one(log)