# Log Slammer

A painfully simple Python app that constantly reads lines from files in a way similar to `tail -f`.  When it finds a line containing valid JSON, it immediately inserts that line into the configured database.

File and databse config can be found in `config/config.json`.  Beware the residual developer-specific paths in this file.

The process includes a PM2 file for simple management.t
