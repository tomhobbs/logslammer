import logging
import threading
import time

import service.log_slammer as ls

logger = logging.getLogger(__name__)

class Tail(threading.Thread):

    def __init__(self, name, file):
        threading.Thread.__init__(self)
        self.name = name
        self.file = open(file, "r")
        self.pleaseStop = False
        self.stopped = False

    def run(self):
        global logger
        logger.info(f'Starting {self.name}')
        self._follow()

    def stop(self):
        global logger
        logger.debug(f'Stopping {self.name}')
        self.pleaseStop = True
        while not self.stopped:
            time.sleep(0.1)
        logger.info(f'Stopped {self.name}')

    def _follow(self):
        global logger
        logger.info(f'Watching {self.file.name}')
        self.file.seek(0, 2)
        while not self.pleaseStop:
            line = self.file.readline()
            if not line:
                time.sleep(0.1)
                continue
            ls.handle_line(line)
        self.stopped = True

