import json
import logging

import dao.logs as logs

logger = logging.getLogger(__name__)


def handle_line(line):
    try:
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f'Handling: {line}')
        as_json = json.loads(line)
        logger.info('Saving new line')
        logs.save(as_json)
    except json.decoder.JSONDecodeError:
        # Otherwise, ignore invalid input
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f'Not valid JSON: {line}')
